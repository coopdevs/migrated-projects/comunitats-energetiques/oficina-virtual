import React from "react";
import { getProductsList } from "lib/api/products";
import { createTicket } from "lib/api/tickets";
import { ProductPickerModal } from "./ProductPicker";
import { useTranslation } from "react-i18next";
import { Text } from "components/Text";

const formatDataPlanSize = (mb) => {
  if (mb < 1024) {
    return `${mb} MB`;
  }

  return `${mb / 1024} GB`;
};

export const AdditionalDataModal = ({ isOpen, onClose, subscription }) => {
  const { t } = useTranslation();

  const onSubmit = async (selectedProduct) => {
    return createTicket({
      body: "-",
      subject: "-",
      meta: [
        {
          key: "ticket_type",
          value: "additional_data",
        },
        {
          key: "phone_number",
          value: subscription.description,
        },
        {
          key: "new_product_code",
          value: selectedProduct.code,
        },
      ],
    });
  };

  return (
    <ProductPickerModal
      title={t("subscriptions.detail.additional_data")}
      submitText={t("subscriptions.detail.additional_data_modal.add_package")}
      selectedText={t(
        "subscriptions.detail.additional_data_modal.selected_package"
      )}
      descriptionText={t(
        "subscriptions.detail.additional_data_modal.description"
      )}
      confirmText={() => {
        return (
          <Text>{t("subscriptions.detail.additional_data_modal.confirm")}</Text>
        );
      }}
      isOpen={isOpen}
      onClose={onClose}
      subscription={subscription}
      getProducts={() =>
        getProductsList({
          subscriptionId: subscription.id,
          productType: "additional_data",
        })
      }
      renderProductDescription={(product) => (
        <span>{formatDataPlanSize(Number(product.data_included))}</span>
      )}
      onSubmit={onSubmit}
      onClickClose={onClose}
    />
  );
};
