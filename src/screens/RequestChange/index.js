import React, { useCallback, useEffect, useState } from "react";
import { SidebarLayout } from "components/layouts/SidebarLayout";
import { Spinner } from "components/Spinner";
import { useAsync } from "react-async-hook";
import { getSubscriptionList } from "lib/api/subscriptions";
import {
  Switch,
  Route,
  useRouteMatch,
  Redirect,
  useHistory
} from "react-router-dom";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import { RequestChangeContext, useRequestChangeContext } from "./context";
import { createTicket } from "lib/api/tickets";
import Alert from "@material-ui/lab/Alert";

import { useTranslation } from "react-i18next";
import { Link } from "components/Link";
import { Stack } from "components/layouts/Stack";

import { SelectScope } from "./steps/SelectScope";
import { SelectSubscriptions } from "./steps/SelectSubscriptions";
import { SelectTopic } from "./steps/SelectTopic";
import { ChangeIban } from "./steps/ChangeIban";
import { ChangeEmail } from "./steps/ChangeEmail";

const SuccessResult = () => {
  const { t } = useTranslation();

  return (
    <Stack>
      <Alert severity="success">{t("request_change.success")}</Alert>
      <Link to="/">{t("common.back_to_home")}</Link>
    </Stack>
  );
};

const ErrorResult = () => {
  const { t } = useTranslation();

  return (
    <Stack>
      <Alert severity="error">{t("common.errors.request_failed")}</Alert>
      <Link to="/request-change">{t("common.start_again")}</Link>
    </Stack>
  );
};

const InitialRedirect = () => {
  const { subscriptions, setWentThroughFirstStep, setConfirmNonce } = useRequestChangeContext();
  const { path } = useRouteMatch();
  const { t } = useTranslation();

  useEffect(() => {
    // this is need to prevent weird auto confirmation when navigating to index
    setConfirmNonce(0);
    setWentThroughFirstStep(true);
  }, []);

  if (subscriptions.length <= 0) {
    return <Alert severity="info">{t('request_change.empty_state')}</Alert>
  }

  if (subscriptions.length > 1) {
    return <Redirect to={`${path}/select-scope`} />;
  }

  return <Redirect to={`${path}/select-topic`} />;
};

const WizardRoute = ({ children, ...rest }) => {
  const { wentThroughFirstStep } = useRequestChangeContext();

  return (
    <Route
      {...rest}
      render={({ location }) =>
        !wentThroughFirstStep ? (
          <Redirect
            to={{ pathName: "/request-change", state: { from: location } }}
          />
        ) : (
          children
        )
      }
    />
  );
};

const Content = ({ subscriptions }) => {
  const { path } = useRouteMatch();
  const [selectedSubscriptions, setSelectedSubscriptions] = useState(subscriptions.length > 0 ? [subscriptions[0].code] : []);
  const [scope, setScope] = useState("some");
  const [topic, setTopic] = useState(null);
  const [confirmNonce, setConfirmNonce] = useState(0);
  const [newValue, setNewValue] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const [wentThroughFirstStep, setWentThroughFirstStep] = useState(false);
  const history = useHistory();

  const onConfirm = useCallback(async () => {
    try {
      setIsLoading(true);
      await createTicket({
        body: "",
        subject: "-",
        meta: [
          {
            key: "ticket_type",
            value: `change_${topic}`
          },
          {
            key: "new_value",
            value: newValue
          },
          {
            key: "scope",
            value: scope
          },
          {
            key: "selected_subscriptions",
            value: scope === "all" ? [] : selectedSubscriptions
          }
        ]
      });
      history.push(`${path}/success`);
    } catch (e) {
      history.push(`${path}/error`);
    } finally {
      setIsLoading(false);
    }
  }, [history, newValue, scope, selectedSubscriptions, path, topic]);

  // This is kind of weird
  useEffect(() => {
    if (confirmNonce === 0) {
      return;
    }

    onConfirm();
  }, [confirmNonce, onConfirm]);

  const requestConfirm = () => setConfirmNonce(confirmNonce + 1);

  const contextValue = {
    subscriptions,
    selectedSubscriptions,
    setSelectedSubscriptions,
    scope,
    setScope,
    topic,
    setTopic,
    setNewValue,
    wentThroughFirstStep,
    setWentThroughFirstStep,
    setConfirmNonce,
    requestConfirm,
    onConfirm
  };

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <RequestChangeContext.Provider value={contextValue}>
      <Switch>
        <Route exact path={path}>
          <InitialRedirect />
        </Route>
        <WizardRoute path={`${path}/select-scope`}>
          <SelectScope />
        </WizardRoute>
        <WizardRoute path={`${path}/select-subscriptions`}>
          <SelectSubscriptions />
        </WizardRoute>
        <WizardRoute path={`${path}/select-topic`}>
          <SelectTopic />
        </WizardRoute>
        <WizardRoute path={`${path}/request-change/iban`}>
          <ChangeIban />
        </WizardRoute>
        <WizardRoute path={`${path}/request-change/email`}>
          <ChangeEmail />
        </WizardRoute>
        <WizardRoute path={`${path}/success`}>
          <SuccessResult />
        </WizardRoute>
        <WizardRoute path={`${path}/error`}>
          <ErrorResult />
        </WizardRoute>
      </Switch>
    </RequestChangeContext.Provider>
  );
};

export const RequestChange = () => {
  const subscriptions = useAsync(getSubscriptionList, []);

  return (
    <SidebarLayout>
      {subscriptions.loading ? (
        <Spinner />
      ) : (
        <Box component={Card} p={[2, 4]}>
          <Content subscriptions={subscriptions.result.data} />
        </Box>
      )}
    </SidebarLayout>
  );
};
