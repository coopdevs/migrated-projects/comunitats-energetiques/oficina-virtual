import React from "react";
import { useTranslation } from "react-i18next";
import { Card } from "components/Card";
import { Text } from "components/Text";
import { Button } from "components/Button";
import { Stack } from "components/layouts/Stack";
import { Tiles } from "components/layouts/Tiles";
import { formatDataPlanSize, formatMinutes, formatBandwith } from "lib/helpers";
import Box from "@material-ui/core/Box";

export const PriceTag = ({ price }) => {
  const { t } = useTranslation();
  const priceInCents = price * 100;
  const integer = Math.trunc(priceInCents / 100);
  const cents = priceInCents % 100;

  return (
    <Box
      height="100%"
      display="flex"
      mt={1}
      flexDirection="column"
      justifyContent="center"
    >
      <Box height="2.25rem" display="flex" flexDirection="row">
        <Box>
          <Text lineHeight="2.25rem" bold size="3xl">
            {integer}
          </Text>
        </Box>
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="space-between"
        >
          <Text lineHeight="1rem" limitToCap size="lg">
            ,{cents.toString().padStart(2, "0")}
          </Text>
          <Text lineHeight="0.65rem" size="sm">
            €/mes
          </Text>
        </Box>
      </Box>
      <div style={{ opacity: 0.5 }}>
        <Box mt={1}>
          <Stack spacing={0}>
            <Text lineHeight="1.0" size="xs">
              {t("price_tag.vat_included")}
            </Text>
            <Text lineHeight="1.0" size="xs">
              {t("price_tag.no_binding")}
            </Text>
          </Stack>
        </Box>
      </div>
    </Box>
  );
};

const CircleCheckIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle cx="10" cy="10" r="9.5" stroke="#F6DE59" />
    <path d="M5.88281 9.57646L8.997 13.5294L14.7063 6.47058" stroke="#F6DE59" />
  </svg>
);

const Reason = ({ text }) => (
  <Box display="flex" alignItems="center">
    <CircleCheckIcon />
    <Box ml={2}>
      <Text color="white">{text}</Text>
    </Box>
  </Box>
);

export const PricePreview = ({
  tariff,
  category,
  details = ["minutes", "data"],
  showSubmit = false,
  onSubmit
}) => {
  const { t } = useTranslation();

  return (
    <Tiles columns={showSubmit ? 2 : 1} spacing={4}>
      <Card variant="noGutter" shadow={false} isHighlighted>
        <Tiles columns={!showSubmit ? 2 : 1}>
          <Box display="flex" flex={1} paddingX={4} paddingY={4}>
            <Box flex={1}>
              <Stack spacing={0}>
                <Box mb={1}>
                  <Text size="xs">{t(`price_tag.category.${category}`)}</Text>
                </Box>
                {details.includes("bandwidth") && (
                  <Text lineHeight="1.0" bold size="xl">
                    {formatBandwith(tariff?.bandwidth)}
                  </Text>
                )}
                {details.includes("minutes") && (
                  <Text lineHeight="1.0" bold size="xl">
                    {formatMinutes(tariff?.minutes)}
                  </Text>
                )}
                {details.includes("data") && (
                  <Text lineHeight="1.0" bold size="xl">
                    + {formatDataPlanSize(tariff?.data)}
                  </Text>
                )}
                {details.includes("totalAmount") && (
                  <Text lineHeight="1.0" bold size="xl">{t(`price_tag.totalAmount`)}</Text>
                )}
              </Stack>
            </Box>
            <Box display="flex" flex={1} alignItems="center">
              <PriceTag price={tariff?.price} />
            </Box>
          </Box>
          {!showSubmit && (
            <Box
              bgcolor="info.main"
              flex={1}
              flexDirection="column"
              justifyContent="center"
              paddingX={4}
              paddingY={4}
              display="flex"
            >
              <Stack spacing={1}>
                <Reason text={t("price_tag.reason_1")} />
                <Reason text={t("price_tag.reason_2")} />
                <Reason text={t("price_tag.reason_3")} />
              </Stack>
            </Box>
          )}
        </Tiles>
      </Card>
      {showSubmit && (
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="flex-end"
          width="100%"
        >
          <Button type="submit" onClick={onSubmit} >{t("common.contract")}</Button>
        </Box>
      )}
    </Tiles>
  );
};
