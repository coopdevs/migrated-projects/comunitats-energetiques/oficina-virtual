import { get } from "axios";

export const getProductsList = ({ subscriptionId, productType }) =>
  get(
    `/api/products/?subscription_id=${subscriptionId}&product_type=${productType}`
  );
