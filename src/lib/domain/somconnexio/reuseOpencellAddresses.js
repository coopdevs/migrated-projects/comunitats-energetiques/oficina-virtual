import { states } from "lib/domain/somconnexio/selections";
import { uniqWith, isEqual } from 'lodash'

const getStateByValue = value => {
  const state = states.find(item => item.label === value);
  return state ? state.value : "";
};

export const mapLegacyAddress = address => {
  return {
    zip_code: address.zip_code,
    street: address.address,
    city: address.city,
    state: getStateByValue(address.subdivision),
    country: "ES"
  };
};

export const reuseOpencellAddresses = addresses => {
  return uniqWith(addresses, isEqual).map(mapLegacyAddress)
}
